const Concept = require('../../models/concept');
const Information = require('../../models/information');
const assert = require('chai').assert;

describe('Suit: Concept&InformationIntegration', function(){
    it('SettingInformationArraySetsSchemaArray', function(){

        //Arrange
        const informationString = "This is an information string";
        const information = new Information();
        information.text = informationString;
        const concept = new Concept();

        //Act
        concept.informationArray = [information];

        //Assert 
        assert.equal(concept.informationArray.length, 1);
    });
});