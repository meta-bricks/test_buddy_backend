const Concept = require('../../models/concept');
const assert = require('chai').assert;
const sinon = require('sinon');
const INVALID_INFORMATION_ERROR_MESSAGE = require('../../models/utilties').INVALID_CONCEPT_ERROR_MESSAGE;



const MockInformation = function(errorMessage){
    this.errorMessage = errorMessage;
    
};

const MockInformationWithText = function(text){
    this.text = sinon.fake.returns(text);
};

describe('Suit: ConceptModelValidation', function(){

    it('FilledInformationArrayNoError', function (){
        //Arrange 
       //const goodMockInformation = new MockInformation("");
        const concept = new Concept();
        const informationString = "This is an information string" ;
       //const goodMockInformation = {text: informationString};

        //Act 
        concept.informationArray = [informationString] ;
        
        //Assert 
        assert.isTrue(concept.errorMessage === "");
    });

    it('InvalidInformationHaveError', function (){
        // Arrange
        //const badMockInformation = new MockInformation(false);
        //const badMockInformation = {text: ""};
        const concept = new Concept();
        //const badInformationString = "";

        //Act
        concept.informationArray = [""];

        //Assert
        assert.isFalse(concept.errorMessage === "");
    });

    it('InvalidConceptErrorMessageCorrect', function(){
        //Arrange 
        //const badMockInformation = new MockInformation(false);
        ///const badMockInformation = {text: ""};
        const concept = new Concept();
        //const badInformationString = "";

        //Act
        concept.informationArray = [""];

        //Assert 
        assert.equal(concept.errorMessage,INVALID_INFORMATION_ERROR_MESSAGE);
    });

});

describe('Suit: ConceptModelCreation', function(){
    
    it('SettingInformationArray', function(){
        //Arrange
        const informationString = "This is an information string";
        const mockInformationWithText = MockInformationWithText(informationString);
        const concept = new Concept();

        //Act
        concept.informationArray = [mockInformationWithText];

        //Assert
        assert.equal(concept.ConcepctDocument.informationArray[0], informationString );

    });

    it('InformationArrayIsComplete', function(){
        //Arrange
        const informationString = "This is an information string";
        const mockInformationWithText = MockInformationWithText(informationString);
        const concept = new Concept();

        //Act
        concept.informationArray = [mockInformationWithText];

        //Assert
        assert.isTrue(concept.ConcepctDocument.informationArray.length === 1 );

    });
});