const Information = require('../../models/information');
const assert = require('chai').assert;
const INVALID_INFORMATION_ERROR_MESSAGE = require('../../models/utilties').INVALID_CONCEPT_ERROR_MESSAGE;



describe('Suit: InformationModelValidation', function () {

    it('EmptyTextHaveError', function () {
        //Arrange
        const testInformation = new Information();
        
        //Act
        testInformation.text = "";
        

        // Assert 
        assert.isFalse(testInformation.errorMessage === "");
    });

    it('EmptyTextErrorMessageCorrect', function() {
          //Arrange
          const testInformation = new Information();
          //Act
          testInformation.text = "";
          
  
          // Assert 
          assert.equal(testInformation.errorMessage, INVALID_INFORMATION_ERROR_MESSAGE);

    });

    it('FilledTextNoError', function(){

        //Arrane
        const testInformation = new Information();
      const testInformationString = "This is a test information string";


        //Act
      testInformation.text = testInformationString;

        //Assert
        assert.isTrue(testInformation.errorMessage === "");
    });

});

describe('Suit: InformationModelCreation', function(){
    it ('SettingText', function(){
        //Arrange
        const testInformation = new Information();
        const testInformationString = "This is a test information string";

        //Act
        testInformation.text = testInformationString;

        //Assert
        assert.equal(testInformation.text, testInformationString);
    });

});