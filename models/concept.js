const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const Information = require('../models/information');
const INVALID_CONCEPT_ERROR_MESSAGE = require('./utilties').INVALID_CONCEPT_ERROR_MESSAGE;

//Mongoose Schema for the Concept model
const concept = new Schema({
    name : { type: String},
   // informationArray : [{type : Schema.Types.ObjectId, ref: 'Information'}]
   informationArray: {
       type: [String], 
       required : [true, INVALID_CONCEPT_ERROR_MESSAGE] }
});

//Concept mongoose model
const ConceptModel = mongoose.model('Concept', concept);

function Concept(){

    //Instance variable for Concept mongoose model
    this.ConcepctDocument = new ConceptModel();
    //Setting to empty array forcing not setting setting information array to through an error
    this.ConcepctDocument.informationArray = [];
    //informationArray
    var informationArray = [];
    Object.defineProperty(this, 'informationArray', {
        get : function(){
            return informationArray;
        },
        set : function(value){
            informationArray = value;
            if (this.areAllInformationObjectsValid()){

                var tempInformationDocArray = [];
                var information = new Information();
                for ( information of informationArray){
                    tempInformationDocArray.push({text: information.text});
                }
                this.ConcepctDocument.informationArray = tempInformationDocArray ;
            }
           
            //this.ConcepctDocument.informationArray = value;
        }
    });

    //error message for validation
    var errorMessage = "";
    Object.defineProperty(this, 'errorMessage', {
        get: function(){

            if (this.ConcepctDocument.validateSync()){
                errorMessage = INVALID_CONCEPT_ERROR_MESSAGE;
            }else{
                errorMessage = "";
            }
            return errorMessage;
        }
    });


    //function for checking if the concept's information objects are valid
    this.isConceptValid = function (){
       /* var information = new Information();
        for (information of this.informationArray){
            if (information.text !== ""){
                errorMessage = INVALID_INFORMATION_ERROR_MESSAGE;
                return false;
            }
        } */

        return true;
    };

}


module.exports = Concept;