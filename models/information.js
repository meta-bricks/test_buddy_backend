const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const INVALID_INFORMATION_ERROR_MESSAGE = require('./utilties').INVALID_CONCEPT_ERROR_MESSAGE;


//Mongoos Schema for Information model
const informationSchema =  new Schema({
    text: {
        type: String,
        required: true
    }

});
//Information mongoose model
const InformationModel = mongoose.model('Information', informationSchema);

function Information(){
    
     //error message for validation
     var errorMEssage = "";
     Object.defineProperty(this, 'errorMessage',{
         get: function(){
             if(this.InformationDocument.validateSync()){
                 errorMEssage = INVALID_INFORMATION_ERROR_MESSAGE;
             }else{
                 errorMEssage = "";
             }
             return errorMEssage;
         }
     });
   
    
    //instance variable for Information mongoose model
    this.InformationDocument = new InformationModel({
        text: ""});
    errorMEssage = this.InformationDocument.validateSync().errors.text.message;
    //text property that get and set the document text
    Object.defineProperty(this, 'text', {
        get: function(){
            return this.InformationDocument.text;
        },
        set: function(value){
            //setting instance document varible text value
            this.InformationDocument.text = value;
        }
    });

    
}


module.exports = Information;