

const ERROR_MESSAGES = {
    INVALID_CONCEPT_ERROR_MESSAGE : "Concept has an empty information string"
};

module.exports = ERROR_MESSAGES;